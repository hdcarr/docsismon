#!/usr/bin/env python

"""
TP-Link cable modem scraper.
"""

from selenium import webdriver
#from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

import json
import pprint

from time import sleep

url = 'http://192.168.100.1/'
## TP-Link has an emulator available online
#url = 'https://emulator.tp-link.com/CableSimulator__TC-7610_V2_Apr-19_09-33-04/index.htm'

options = Options()
options.headless = True
#driver = webdriver.Firefox(options=options,executable_path='/usr/local/bin/geckodriver')
driver = webdriver.Chrome(options=options)

driver.get(url)
# ISSUE: figure out how to wait on elements to load instead of forcing sleeps

stats = {}

# Device Information
devinfo_element_id = 'devInfoTbl'
fetch_delay = 3
ignored_exceptions=(NoSuchElementException,StaleElementReferenceException)
dev_table = WebDriverWait(driver, fetch_delay,ignored_exceptions=ignored_exceptions)\
                        .until(expected_conditions.presence_of_element_located((By.ID, devinfo_element_id))).find_elements_by_class_name('table-content')

stats['devinfo'] = []

specification = dev_table[1].get_attribute("textContent")
hardware_ver = dev_table[3].get_attribute("textContent")
software_ver = dev_table[5].get_attribute("textContent")
modem_mac_adr = dev_table[7].get_attribute("textContent")
modem_serial = dev_table[9].get_attribute("textContent")
# don't think any of these are worth tracking and passing to influx
# either remove or maybe only pass if they've changed?
stats['devinfo'].append({
    'specification': specification,
    'hardware_ver': hardware_ver,
    'software_ver': software_ver,
    'modem_mac_adr': modem_mac_adr,
    'modem_serial': modem_serial
    })

# Network Information
sysinfo_buttons = driver.find_elements_by_class_name('ml2')
sysinfo_buttons[1].click()

sysinfo_element_id = 'sysInfoTbl'
sysinfo_table = WebDriverWait(driver, fetch_delay,ignored_exceptions=ignored_exceptions)\
                        .until(expected_conditions.presence_of_element_located((By.ID, sysinfo_element_id))).find_elements_by_class_name('table-content')
#sysinfo_table = driver.find_elements_by_class_name('table-content')

stats['netinfo'] = []

# TODO: parse into time objects?
uptime = sysinfo_table[1].get_attribute("textContent")
sys_time = sysinfo_table[3].get_attribute("textContent")
net_access = sysinfo_table[5].get_attribute("textContent")
access_duration = sysinfo_table[7].get_attribute("textContent")
access_expire = sysinfo_table[9].get_attribute("textContent")

stats['netinfo'].append({
    'uptime': uptime,
    'sys_time': sys_time,
    'net_access': net_access,
    'access_duration': access_duration,
    'access_expire': access_expire
    })


# Advanced
adv_button = driver.find_element_by_id('advanced')
adv_button.click()
driver.implicitly_wait(5) # wait 3 seconds to allow table to populate - this apparently isn't doing anything? still have to sleep...
sleep(2)
# Startup Procedure Table
stats['startup'] = []

proctbl_element_id = 'ProcTbl'
proc_table = WebDriverWait(driver, fetch_delay,ignored_exceptions=ignored_exceptions)\
                        .until(expected_conditions.presence_of_element_located((By.ID, proctbl_element_id))).find_elements_by_class_name('table-content')
#proc_table = driver.find_element_by_id('ProcTbl').find_elements_by_class_name('table-content')
downstream_channel = proc_table[1].get_attribute("textContent")
downstream_status = proc_table[2].get_attribute("textContent")
connectivity_state = proc_table[4].get_attribute("textContent")
boot_state = proc_table[7].get_attribute("textContent")
security = proc_table[10].get_attribute("textContent")

stats['startup'].append({
    'downstream_channel': downstream_channel,
    'downstream_status': downstream_status,
    'connectivity_state': connectivity_state,
    'boot_state': boot_state,
    'security': security
    })

# # Downstream Table
# stats['downstream'] = []

# dstbl_element_id = 'DsTbl'
# ds_rows = WebDriverWait(driver, fetch_delay,ignored_exceptions=ignored_exceptions)\
#                         .until(expected_conditions.presence_of_element_located((By.ID, dstbl_element_id))).find_elements_by_tag_name('tr')
# #ds_rows = driver.find_element_by_id('DsTbl').find_elements_by_tag_name('tr')
# # table head also has a 'tr' tag, so we have to pop it off to only look at content rows
# # ISSUE: is there a cleaner way to do this?
# ds_rows.pop(0)

# # ISSUE: for some reason, only rows 1-4 are populated, 5-8 are empty
# # TODO: split out column text to remove units?
# for rows in ds_rows:
#     row = rows.find_elements_by_class_name('table-content')
#     channel = row[0].get_attribute("textContent")
#     status = row[1].get_attribute("textContent")
#     modulation = row[2].get_attribute("textContent")
#     channel_id = row[3].get_attribute("textContent")
#     frequency = row[4].get_attribute("textContent")
#     power = row[5].get_attribute("textContent")
#     snr = row[6].get_attribute("textContent")
#     channelstats = {'channel': channel,
#                     'status': status,
#                     'modulation': modulation,   
#                     'channel_id': channel_id,
#                     'frequency': frequency,
#                     'power': power,
#                     'snr': snr}
#     stats['downstream'].append(channelstats)

# # Upstream Table
# stats['upstream'] = []
# ustbl_element_id = 'UsTbl'
# us_rows = WebDriverWait(driver, fetch_delay,ignored_exceptions=ignored_exceptions)\
#                         .until(expected_conditions.presence_of_element_located((By.ID, ustbl_element_id))).find_elements_by_tag_name('tr')
# #us_rows = driver.find_element_by_id('UsTbl').find_elements_by_tag_name('tr')
# # table head also has a 'tr' tag, so we have to pop it off to only look at content rows
# # ISSUE: is there a cleaner way to do this?
# us_rows.pop(0)
# # ISSUE: for some reason, only rows 1-4 are populated, 5-8 are empty
# # TODO: split out column text to remove units?
# for rows in us_rows:
#     row = rows.find_elements_by_class_name('table-content')
#     channel = row[0].get_attribute("textContent")
#     status = row[1].get_attribute("textContent")
#     channel_type = row[2].get_attribute("textContent")
#     channel_id = row[3].get_attribute("textContent")
#     symbol_rate = row[4].get_attribute("textContent")
#     power = row[5].get_attribute("textContent")
#     channelstats = {'channel': channel,
#                     'status': status,
#                     'channel_type': channel_type,
#                     'channel_id': channel_id,
#                     'symbol_rate': symbol_rate,
#                     'power': power
#                     }
#     stats['upstream'].append(channelstats)

#pprint.pprint(stats)
print("modem_stats devinfo.hardware_ver=" + stats['devinfo'][0]['hardware_ver'] + 
        ", devinfo.modem_mac_adr=" + stats['devinfo'][0]['modem_mac_adr'] + 
        ", devinfo.modem_serial=" + stats['devinfo'][0]['modem_serial'] +
        ", devinfo.software_ver=" + stats['devinfo'][0]['software_ver'] +
        ", devinfo.specification=" + stats['devinfo'][0]['specification'] +
        ", netinfo.uptime=" + stats['netinfo'][0]['uptime'] +
        ", netinfo.sys_time=" + stats['netinfo'][0]['sys_time'] +
        ", netinfo.net_access=" + stats['netinfo'][0]['net_access'] +
        ", netinfo.access_duration=" + stats['netinfo'][0]['access_duration'] +
        ", netinfo.access_expire=" + stats['netinfo'][0]['access_expire'] +
        ", startup.downstream_channel=" + stats['startup'][0]['downstream_channel'] +
        ", startup.downstream_status=" + stats['startup'][0]['downstream_status'] +
        ", startup.connectivity_state=" + stats['startup'][0]['connectivity_state'] +
        ", startup.boot_state=" + stats['startup'][0]['boot_state'] +
        ", startup.security=" + stats['startup'][0]['security'])
